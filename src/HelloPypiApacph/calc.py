'''
author: cph
since:  20231215
'''
import math

def printPi():
    print(f'PI = {math.pi}')
    
if __name__ == '__main__':
    printPi()
