'''
author: cph
since:  20231215
'''
import time

def printDT():
    print(f'Date/Time = {time.asctime()}')
    
if __name__ == '__main__':
    printDT()
