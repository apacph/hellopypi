'''
author: cph
since:  20231215
'''
import HelloPypiApacph as hp
from HelloPypiApacph import calc, dt

if __name__ == '__main__':
    print(f'HelloPypiApacph version = {hp.__version__}')
    hp.print2()
    calc.printPi()
    dt.printDT()
    