############
Basics
############

.. figure:: pierre-chatel-innocenti-pxoZSTdAzeU-unsplash.jpg
    :align: center
    :width: 600px
    
    Photo by Pierre Châtel-Innocenti on Unsplash
  
.. note:: Outline 

    #. `Introduction <#introduction>`__
    #. `Examples <#examples>`__

************
Introduction
************
#. We show introduction here...

********
Examples
********
#. We show examples here...


.. note::

    #. Start: 20120311
    #. System Environment:

        .. literalinclude:: ../requirements.txt
            :caption: requirements.txt
            :linenos:
            
        