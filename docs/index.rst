#############
HelloPyPI
#############

.. figure:: pablo-gentile-3MYvgsH1uK0-unsplash.jpg
    :align: center
    :width: 600 px

    Photo by Pablo Gentile on Unsplash
          
.. toctree::
    :caption: Python PyPI
    :name: toc_PythonPyPI
    :maxdepth: 4
    :numbered:
    
    Basics
    

.. note:: System Environment

    #. Start: 20170719
    #. System Environment:
    
        .. literalinclude:: ../requirements.txt
            :caption: requirements.txt
            :linenos: