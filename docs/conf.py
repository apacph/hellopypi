############################################################
# Global Variables 
############################################################
version = 'v20231215'
project = u'WhoNotes: Py.HelloPyPI'
copyright = u'2000-2024, Po-Hsun Cheng, Ph.D.'
author = 'Po-Hsun Cheng, Ph.D.'
release = version

templates_path = ['_templates']
source_suffix = '.rst'
source_encoding = 'utf-8-sig'
master_doc = 'index'
language = 'en'         # 'zh_TW'
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
pygments_style = 'sphinx'
todo_include_todos = False

extensions = []

html_theme_options = {
    'navbar_links': [
         ('Home', 'index', False),
    ]
}

html_baseurl = ''
html_static_path = ['_static']
htmlhelp_basename = 'Python'
html_math_renderer = 'mathjax'
html_use_smartypants = True
html_secnumber_suffix = '. '
html_use_index = False
html_use_opensearch = ''
html_copy_source = True
html_show_sourcelink = False
html_show_sphinx = True
html_show_copyright = True

############################################################
# Function: Global Variables Definition Area
############################################################
def ultimateReplace(app, docname, source):
    result = source[0]
    for key in app.config.ultimate_replacements:
        result = result.replace(key, app.config.ultimate_replacements[key])
    source[0] = result

def setup(app):
    #---------------------------------------------
    # sphinx-material Theme: 
    #---------------------------------------------
    app.add_css_file('_static/custom.css')
    app.add_config_value('ultimate_replacements', {}, True)
    app.connect('source-read', ultimateReplace)
    